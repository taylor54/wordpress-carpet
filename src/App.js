import logo from "./logo.svg";
import "./App.css";

import React, { useState, useEffect, useRef } from "react";

import ReactCrop, {
	centerCrop,
	makeAspectCrop,
	Crop,
	PixelCrop,
} from "react-image-crop";
import { canvasPreview } from "./canvasPreview";
import { useDebounceEffect } from "./useDebounceEffect";

import "react-image-crop/dist/ReactCrop.css";

function centerAspectCrop(mediaWidth, mediaHeight, aspect) {
	return centerCrop(
		makeAspectCrop(
			{
				unit: "%",
				width: 90,
			},
			aspect,
			mediaWidth,
			mediaHeight
		),
		mediaWidth,
		mediaHeight
	);
}

function App() {
	const [imgSrc, setImgSrc] = useState("");
	const previewCanvasRef = useRef(null);
	const imgRef = useRef(null);
	const hiddenAnchorRef = useRef(null);
	const blobUrlRef = useRef("");
	const carpetCanvasRef = useRef(null);
	const [crop, setCrop] = useState();
	const [completedCrop, setCompletedCrop] = useState();
	const [scale, setScale] = useState(1);
	const [rotate, setRotate] = useState(0);
	const [aspect, setAspect] = useState(16 / 9);
	const [colours, setColours] = useState([]);

	function onSelectFile(e) {
		if (e.target.files && e.target.files.length > 0) {
			setCrop(undefined); // Makes crop preview update between images.
			const reader = new FileReader();
			reader.addEventListener("load", () =>
				setImgSrc(reader.result?.toString() || "")
			);
			reader.readAsDataURL(e.target.files[0]);
		}
	}

	function onImageLoad(e) {
		if (aspect) {
			const { width, height } = e.currentTarget;
			setCrop(centerAspectCrop(width, height, aspect));
		}
	}

	function onDownloadCropClick() {
		if (!previewCanvasRef.current) {
			throw new Error("Crop canvas does not exist");
		}
		console.log(previewCanvasRef.current.toDataURL());
		previewCanvasRef.current.toDataURL((uri) => {
			console.log("uri: ", uri);
			// if (!blob) {
			// 	throw new Error("Failed to create blob");
			// }
			// if (blobUrlRef.current) {
			// 	URL.revokeObjectURL(blobUrlRef.current);
			// }
			// blobUrlRef.current = URL.createObjectURL(blob);
			// hiddenAnchorRef.current.href = blobUrlRef.current;
			// hiddenAnchorRef.current.click();
		});
	}

	useDebounceEffect(
		async () => {
			if (
				completedCrop?.width &&
				completedCrop?.height &&
				imgRef.current &&
				previewCanvasRef.current
			) {
				console.log("cropped ", completedCrop, previewCanvasRef.current);
				// We use canvasPreview as it's much faster than imgPreview.
				canvasPreview(
					imgRef.current,
					previewCanvasRef.current,
					completedCrop,
					scale,
					rotate
				);
				// noise(previewCanvasRef.current.getContext("2d"))
			}
		},
		100,
		[completedCrop, scale, rotate]
	);

	function handleToggleAspectClick() {
		if (aspect) {
			setAspect(undefined);
		} else if (imgRef.current) {
			const { width, height } = imgRef.current;
			setAspect(16 / 9);
			setCrop(centerAspectCrop(width, height, 16 / 9));
		}
	}

	const [sizes, setSizes] = useState([]);
	const fetchUser = () => {
		fetch("https://live.mtapi.biz/v1/designs/sizes", {
			method: "GET",
			headers: {
				"ocp-apim-developer-key": "9c480f48dbdd29145d40c889d07df3a4",
				"ocp-apim-subscription-key": "f75f141ee40345d389fea2dd964598b9",
			},
		})
			.then((response) => response.json())
			.then((response) => {
				console.log(response);
				// let newData = { ...response };
				setSizes(response);
			})
			.catch((error) => {
				console.error(error);
			});
		fetch("https://live.mtapi.biz/v1/designs/colors?itemNumber=741", {
			method: "GET",
			headers: {
				"ocp-apim-developer-key": "9c480f48dbdd29145d40c889d07df3a4",
				"ocp-apim-subscription-key": "f75f141ee40345d389fea2dd964598b9",
			},
		})
			.then((response) => response.json())
			.then((response) => {
				console.log(response);
				setColours(response);
			})
			.catch((error) => {
				console.error(error);
			});
	};

	useEffect(() => {
		fetchUser();
	}, []);

	const [canvasX, setCanvasX] = useState(500);
	const [canvasY, setCanvasY] = useState(300);
	const setCanvasSize = (size) => {
		console.log(size);
		const values = size.split("x").map((s) => parseInt(s));
		console.log(values);

		setCanvasX(values[1]);
		setCanvasY(values[0]);
		const canvas = carpetCanvasRef.current;
		canvas.width = window.innerWidth * window.devicePixelRatio;
		canvas.height = window.innerHeight * window.devicePixelRatio;

		canvas.style.width = window.innerWidth + "px";
		canvas.style.height = window.innerHeight + "px";

		const ctx = carpetCanvasRef.current.getContext("2d");

		const dataUri = previewCanvasRef.current.toDataURL();
		var image = new Image();

		image.addEventListener(
			"load",
			function () {
				console.log("image load");
				var canvasContext = ctx;

				var wrh = image.width / image.height;
				console.log("wrh", wrh);
				var newWidth = canvas.width / wrh;
				var newHeight = canvas.height / wrh;
				if (newHeight > canvas.height) {
					newHeight = canvas.height;
					newWidth = newHeight * wrh;
				}
				console.log(newHeight, newWidth);
				var xOffset =
					newWidth < canvas.width ? (canvas.width - newWidth) / 2 : 0;
				var yOffset =
					newHeight < canvas.height ? (canvas.height - newHeight) / 2 : 0;
				ctx.strokeStyle = "black";
				ctx.lineWidth = 25;
				ctx.strokeRect(0, 0, canvas.width, canvas.height);
				noise(ctx);
				canvasContext.drawImage(image, xOffset, yOffset, newWidth, newHeight);
			},
			false
		);

		console.log("loggi ", dataUri);
		image.src = dataUri;

		// var background = new Image();
		// background.src = "https://djrib5zb6jnmn.cloudfront.net/Colors/7417/6.jpg";
		// background.width = 5;
		// background.height = 5;
		// background.onload = () => {
		// 	const ctx = carpetCanvasRef.current.getContext("2d");
		// 	let ptrn = ctx.createPattern(background, "repeat");
		// 	ctx.fillStyle = ptrn;
		// 	ctx.fillRect(0, 0, values[0], values[1]);
		// 	// .drawImage(background, 0, 0);
		// };
	};

	// function noise() {
	//   const ctx = noise(carpetCanvasRef.current.getContext("2d"))
	// 	const w = ctx.canvas.width,
	// 		h = ctx.canvas.height,
	// 		iData = ctx.createImageData(w, h),
	// 		buffer32 = new Uint32Array(iData.data.buffer),
	// 		len = buffer32.length;
	// 	let i = 0;

	// 	for (; i < len; i++) if (Math.random() < 0.1) buffer32[i] = 0xffffffff;

	//   ctx.fillStyle = "blue";
	//   ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
	// 	ctx.putImageData(iData, 0, 0);
	// }

	function noise(ctx) {
		const w = ctx.canvas.width,
			h = ctx.canvas.height,
			iData = ctx.getImageData(0, 0, w, h),
			// iData = ctx.createImageData(w, h),
			buffer32 = new Uint32Array(iData.data.buffer),
			len = buffer32.length;
		let i = 0;

		for (; i < len; i++) {
			if (buffer32[i] !== 0) {
				console.log(buffer32[i]);
			}
			if (Math.random() < 0.2) buffer32[i] = 0x80ffffff;
		}

		ctx.putImageData(iData, 0, 0);
	}

	const [carpetColour, setCarpetColour] = useState();
	const onSetCarpetColour = (red, green, blue) => {
		function componentToHex(c) {
			var hex = c.toString(16);
			return hex.length == 1 ? "0" + hex : hex;
		}
		function rgbToHex(r, g, b) {
			return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
		}
		setCarpetColour(rgbToHex(red, green, blue))
	}

	return (
		<div styles="{{width: '500px'}}">
			{/* {__("Demo Api Block – hello from the editor!", "demo-api-block")} */}
			<div className="Crop-Controls">
				<input type="file" accept="image/*" onChange={onSelectFile} />
				<div>
					<label htmlFor="scale-input">Scale: </label>
					<input
						id="scale-input"
						type="number"
						step="0.1"
						value={scale}
						disabled={!imgSrc}
						onChange={(e) => setScale(Number(e.target.value))}
					/>
				</div>
				<div>
					<label htmlFor="rotate-input">Rotate: </label>
					<input
						id="rotate-input"
						type="number"
						value={rotate}
						disabled={!imgSrc}
						onChange={(e) =>
							setRotate(Math.min(180, Math.max(-180, Number(e.target.value))))
						}
					/>
				</div>
				<div>
					<button onClick={handleToggleAspectClick}>
						Toggle aspect {aspect ? "off" : "on"}
					</button>
				</div>
			</div>
			{!!imgSrc && (
				<ReactCrop
					crop={crop}
					onChange={(_, percentCrop) => setCrop(percentCrop)}
					onComplete={(c) => setCompletedCrop(c)}
					aspect={aspect}
					styles={{ width: "500px" }}
				>
					<img
						ref={imgRef}
						alt="Crop me"
						src={imgSrc}
						style={{ transform: `scale(${scale}) rotate(${rotate}deg)` }}
						onLoad={onImageLoad}
					/>
				</ReactCrop>
			)}
			{!!completedCrop && (
				<>
					<div>
						<canvas
							ref={previewCanvasRef}
							style={{
								border: "1px solid black",
								objectFit: "contain",
								width: completedCrop.width,
								height: completedCrop.height,
							}}
						/>
						<div style={{ display: "flex" }}>
							<button onClick={() => setCanvasSize("300 x 500")}>
								default
							</button>

							{sizes && (
								<>
									{sizes.map((size, idx) => {
										return (
											<button
												onClick={() => setCanvasSize(size.Name)}
												key={idx}
											>
												{size.Name}
											</button>
										);
									})}
								</>
							)}
						</div>
						<div style={{ display: 'flex'}}>
						{colours && (
								<>
									{colours.map((colour, idx) => {
										return (
											<div
												style={{
													width: "40px",
													height: "40px",
													backgroundColor: `rgb(${colour.Red}, ${colour.Green}, ${colour.Blue})`,
												}}
												onClick={() => onSetCarpetColour(colour.Red, colour.Green, colour.Blue)}
											>
											</div>
										);
									})}
								</>
							)}
						</div>
						<canvas
							ref={carpetCanvasRef}
							style={{
								// border: "3px solid black",
								margin: 10,
								width: canvasX,
								height: canvasY,
								background: carpetColour,
							}}
						/>
					</div>
					<div>
						<button onClick={onDownloadCropClick}>Download Crop</button>
						<a
							ref={hiddenAnchorRef}
							download
							style={{
								position: "absolute",
								top: "-200vh",
								visibility: "hidden",
							}}
						>
							Hidden download
						</a>
					</div>
				</>
			)}
		</div>
	);
}

export default App;
